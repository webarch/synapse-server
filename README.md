# Webarchitects Synapse / Matrix / Element Server

This repo has been created to be used to develop Ansible roles for [Synapse Matrix](https://git.coop/webarch/synapse) and [Element](https://git.coop/webarch/element).
