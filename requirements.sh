#!/usr/bin/env bash

if [[ "${1}" ]]; then
  ansible-galaxy install -r requirements.yml --force "${1}"
else
  ansible-galaxy install -r requirements.yml --force
fi

